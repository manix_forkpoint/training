'use strict';

var guard = require("app_storefront_controllers/cartridge/scripts/guard");

function dump(data) {
	response.getWriter().println(data);
}

exports.Start = guard.ensure(["get"], function() {
	var objects = dw.object.CustomObjectMgr.getAllCustomObjects("NotificationRequest").asList();

	response.getWriter().print("asdf");
});

exports.Post = guard.ensure(["post"], function() {
	
	var phone = request.httpParameterMap.phone;
	var UUID = request.httpParameterMap.UUID;
	var hash = UUID + " " + phone;
	
	dw.system.Transaction.wrap(function () {
		try {
    		dw.object.CustomObjectMgr.createCustomObject("NotificationRequest", hash);
		} catch (e) {
			if (e.message.indexOf("Key is not unique")) {
				// this is ok, means it's already saved        				
			} else {
				response.getWriter().print("We couldn't process your request.");
			}
		}
		
		response.getWriter().print("You will be notified as soon as this product is back in stock.");
	});
});