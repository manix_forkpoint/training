'use strict';

var guard = require("app_storefront_controllers/cartridge/scripts/guard");
var app = require('*/cartridge/scripts/app');

request.custom.mmActive = "donors";

function dump(obj) {
    var out = '';
    for (var i in obj) {
        out += "<strong>" + i + "</strong>: " + obj[i] + "<br/>";
    }
    return out;
}

function showForm() {
	dw.template.ISML.renderTemplate("donors/add", {
		ContinueURL : dw.web.URLUtils.url('Donors-Post'),
		CurrentForms : session.forms
	});
}

function render(data) {
	dw.template.ISML.renderTemplate("donors/manage", data);
}

exports.Start = guard.ensure(["get"], render);

exports.Add = guard.ensure(["get"], function() {
	session.forms.add_donor.clearFormElement();
	
	render({
		template: "add",
		ContinueURL : dw.web.URLUtils.url('Donors-Post'),
		CurrentForms : session.forms
	});
});

exports.Post = guard.ensure(["post", "csrf"], function() {

	var form = app.getForm("add_donor");
	
    form.handleAction({
        insert: function() {
        	// response.getWriter().println(dump(form));

        	dw.system.Transaction.wrap(function () {
        		try {
            		var co = dw.object.CustomObjectMgr.createCustomObject("Donor", form.getValue("email"));
            		form.copyTo(co);
        		} catch (e) {
        			if (e.message.indexOf("Key is not unique")) {
        				form.get("email").invalidate(dw.web.Resource.msg("form.emailtaken", "common", null));        				
        			} else {
        				throw e;
        			}
        		}
        	});

        	if (form.isValid()) {
        		response.getWriter().print("success");
        	} else {
        		showForm();
        	}
        },
        error: function() {
        	showForm();
        }
    });
});

exports.List = guard.ensure(["get"], function() {
	
	render({ 
		template: "list",
		donors: dw.object.CustomObjectMgr.getAllCustomObjects("Donor")
	});
	
});

exports.Brat = guard.ensure(["get"], function() {
	
	var req = new XMLHttpRequest();
	
	response.getWriter().print(req.call({
		url: "https://manix.info"
	}));
});



