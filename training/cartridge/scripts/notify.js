

exports.execute = function () {

	var objects = dw.object.CustomObjectMgr.getAllCustomObjects("NotificationRequest").asList();
	var workingset = {};

	for (var i in objects) {
		var data = objects[i].custom.hash.split(" ");
		var pid = data.shift();
		var phone = data.join("");

		if (!workingset[pid]) {
			workingset[pid] = [objects[i]];
		}

		workingset[pid].push(phone);
	}
	
	if (Object.keys(workingset).length) {
		var service = dw.svc.LocalServiceRegistry.createService("TwilioSMSNotify", {
			createRequest: function(svc: dw.svc.HTTPService, args){
				svc.addHeader("Content-Type", "application/x-www-form-urlencoded");
				return serialize(args);
			},
			parseResponse: function(svc: HTTPService, client: HTTPClient) {
				return client.text;
			}
		});
		
		serialize = function(obj) {
		  var str = [];
		  for(var p in obj)
		    if (obj.hasOwnProperty(p)) {
		      str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
		    }
		  return str.join("&");
		}
		
		for (var pid in workingset) {
			if (!dw.catalog.ProductMgr.getProduct(pid).getAvailabilityModel().isInStock()) {
				continue;
			}

			var destroy = true;

			for (var i = 1, l = workingset[pid].length; i < l; i++) {
				destroy = destroy && ("" + service.call({
					"From": "+12517322189",
					"To": workingset[pid][i],
					"Body": "Product " + pid + " is in stock!"
				})) === "[OK]";
			}
			
			if (destroy) {
				// TODO fix below code, throws null pointer but argument is of type CustomObject
				// dw.object.CustomObjectMgr.remove(workingset[pid][0]);
			}
		}
}