function preorderGetNotified(event, uri, UUID) {
    event.preventDefault();

    var phone = document.getElementById("preorderPhone").value;

    if (!phone) {
        return alert("Please enter phone number");
    }
    
    $.post(uri, {
        phone: phone,
        UUID: UUID
    }, function(resp) {
        $("#notificationRequestPane").html(resp);
    })

    return false;
}